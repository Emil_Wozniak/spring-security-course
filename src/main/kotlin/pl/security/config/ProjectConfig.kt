package pl.security.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy
import org.springframework.http.HttpMethod.OPTIONS
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.password.NoOpPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import pl.security.config.constant.GET
import pl.security.config.constant.POST
import pl.security.security.filter.LoginAuthenticationFilter
import pl.security.security.filter.TokenAuthenticationFilter
import pl.security.security.provider.KeyProvider
import pl.security.security.provider.OTPAuthenticationProvider
import pl.security.security.provider.TokenAuthProvider
import pl.security.security.provider.UsernamePasswordProvider
import org.springframework.web.cors.CorsConfiguration.ALL

@Configuration
class ProjectConfig constructor(
    @Lazy private val loginAuthenticationFilter: LoginAuthenticationFilter,
    @Lazy private val tokenFilter: TokenAuthenticationFilter,
    @Lazy private val usernamePasswordProvider: UsernamePasswordProvider,
    @Lazy private val tokenProvider: TokenAuthProvider,
    @Lazy private val otpProvider: OTPAuthenticationProvider,
    @Lazy private val keyProvider: KeyProvider,
) : WebSecurityConfigurerAdapter() {

    override fun configure(web: WebSecurity) {
        web.ignoring()
            .antMatchers(OPTIONS, "/**")
            .antMatchers("/**/*.{js,html,css}")
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager =
        super.authenticationManagerBean()

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(usernamePasswordProvider)
            .authenticationProvider(otpProvider)
            .authenticationProvider(keyProvider)
            .authenticationProvider(tokenProvider)
    }

    @Bean
    @Suppress("DEPRECATION")
    fun passwordEncoder(): PasswordEncoder = NoOpPasswordEncoder.getInstance()

    override fun configure(http: HttpSecurity) {
        http
            .addFilterAt(loginAuthenticationFilter, BasicAuthenticationFilter::class.java)
            .addFilterAfter(tokenFilter, BasicAuthenticationFilter::class.java)

        http.csrf().disable()
            .authorizeRequests()
            .anyRequest().permitAll()

        /**
         * Create CORS and configure it to allows to call all endpoints on POST and GET
         * methods.
         * THIS SHOULD NEVER BE CONFIGURED ON REAL CASE SCENARIO APP.
         */
        http.cors { cors ->
            cors.configurationSource {
                CorsConfiguration().let { config ->
                    config.allowedMethods = listOf(GET, POST)
                    config.allowedOrigins = listOf(ALL)
                    config
                }
            }
        }
    }
}
