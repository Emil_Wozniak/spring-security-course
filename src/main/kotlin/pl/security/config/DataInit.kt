package pl.security.config

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import pl.security.model.Authority
import pl.security.model.User
import pl.security.repo.UserRepo

@Component
class DataInit(
    private val users: UserRepo,
) : ApplicationRunner {
    override fun run(args: ApplicationArguments?) {
        val user = User("emil", "1234")
        val auths = Authority(null, user, "read")
        user.authorities = listOf(auths)
        users.save(user)
    }
}