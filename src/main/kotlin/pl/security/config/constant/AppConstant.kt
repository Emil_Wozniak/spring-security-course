package pl.security.config.constant

const val AUTHORIZATION = "Authorization"
const val USERNAME = "username"
const val PASSWORD = "password"
const val ONE_TIME_PASSWORD = "otp"

const val EMOJI = "\uD83D\uDE26"

const val GET = "GET"
const val POST = "POST"