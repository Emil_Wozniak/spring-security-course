package pl.security.utils.cypher

import org.apache.commons.codec.binary.Base64.decodeBase64
import org.apache.commons.codec.binary.Base64.encodeBase64
import java.security.spec.KeySpec
import javax.crypto.Cipher
import javax.crypto.Cipher.*
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.DESedeKeySpec

/**
 * This form of data encryption algorithm applies block cipher algorithms
 * thrice to all the data blocks individually.
 */
internal class TripleDES {
    private val keySpec: KeySpec
    private val keyFactory: SecretKeyFactory
    private val cipher: Cipher
    var arrayBytes: ByteArray
    private val encryptionKey = "ThisIsSpartaThisIsSparta"
    private val scheme: String
    var key: SecretKey

    fun encrypt(unencryptedString: String): String? {
        var encryptedString: String? = null
        try {
            cipher.init(ENCRYPT_MODE, key)
            val plainText = unencryptedString.toByteArray(charset(UNICODE_FORMAT))
            val encryptedText: ByteArray = cipher.doFinal(plainText)
            encryptedString = String(encodeBase64(encryptedText))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return encryptedString
    }

    fun decrypt(encryptedString: String?): String? {
        var decryptedText: String? = null
        try {
            cipher.init(DECRYPT_MODE, key)
            val encryptedText: ByteArray = decodeBase64(encryptedString)
            val plainText: ByteArray = cipher.doFinal(encryptedText)
            decryptedText = String(plainText)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return decryptedText
    }

    companion object {
        private const val UNICODE_FORMAT = "UTF8"
        const val DE_SEED_ENCRYPTION_SCHEME = "DESeed"
    }

    init {
        scheme = DE_SEED_ENCRYPTION_SCHEME
        arrayBytes = encryptionKey.toByteArray(charset(UNICODE_FORMAT))
        keySpec = DESedeKeySpec(arrayBytes)
        keyFactory = SecretKeyFactory.getInstance(scheme)
        cipher = getInstance(scheme)
        key = keyFactory.generateSecret(keySpec)
    }
}