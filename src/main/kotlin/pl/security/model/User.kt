package pl.security.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.security.core.userdetails.UserDetails
import javax.persistence.*
import javax.persistence.CascadeType.ALL
import javax.persistence.FetchType.EAGER
import javax.persistence.GenerationType.IDENTITY

@Entity
@Table(name = "users")
data class User(
        @Id
        @GeneratedValue(strategy = IDENTITY)
        var id: Long?,
        @Column(name = "username")
        var username: String,
        @Column(name = "password")
        var password: String,
        @Column(name = "enabled")
        var enabled: Boolean,
        @JsonIgnoreProperties(value = ["user"])
        @OneToMany(cascade = [ALL], fetch = EAGER, mappedBy = "user", orphanRemoval = true)
        var authorities: Collection<Authority>
) {
    @Suppress("UNCHECKED_CAST")
    constructor(details: UserDetails) : this(
            null,
            details.username,
            details.password,
            details.isEnabled,
            details.authorities.map { Authority(null, null, it.authority) }
    )
        constructor(username: String, password: String): this(
                null, username,
                password,
                enabled = true,
                authorities= listOf(Authority(null, null, "read"))
        )

        override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (other !is User) return false

                if (username != other.username) return false
                if (password != other.password) return false
                if (enabled != other.enabled) return false

                return true
        }

        override fun hashCode(): Int {
                var result = username.hashCode()
                result = 31 * result + password.hashCode()
                result = 31 * result + enabled.hashCode()
                return result
        }

        override fun toString(): String {
                return "User(username='$username', password='$password', enabled=$enabled)"
        }

}
