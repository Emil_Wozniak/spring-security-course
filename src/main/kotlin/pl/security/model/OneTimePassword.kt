package pl.security.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "otp")
data class OneTimePassword(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private var id: Long? = null,
    private var username: String = "",
    var oneTimePassword: String = ""
)
