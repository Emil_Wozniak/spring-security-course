package pl.security.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import javax.persistence.*
import javax.persistence.CascadeType.ALL
import javax.persistence.FetchType.EAGER
import javax.persistence.FetchType.LAZY
import javax.persistence.GenerationType.IDENTITY

@Entity
@Table(name = "authorities")
data class Authority(
        @Id
        @GeneratedValue(strategy = IDENTITY)
        var id: Long?,
        @JsonIgnore
        @ManyToOne(cascade = [ALL], fetch = LAZY, optional = true)
        var user: User?,
        @Column(name = "authority")
        private var authority: String
) : GrantedAuthority {
    override fun getAuthority(): String {
        return authority
    }
}