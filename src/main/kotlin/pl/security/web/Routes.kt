package pl.security.web

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.servlet.function.RouterFunction
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.router
import pl.security.handler.RouteHandler

const val ROOT = "/"
const val FORWARD = "/**"

@Configuration
class Routes(private val handler: RouteHandler) {

    @Bean(value = ["allRoutes"])
    fun routes(): RouterFunction<ServerResponse> = router {
        accept(TEXT_HTML).nest {
            GET(ROOT) { ok().render("index") }

        }
        ROOT.nest {
            GET("form") { ok().render("login.html") }
            GET("hello", handler::hello)
            POST("test", handler::test)
            accept(APPLICATION_JSON).nest {
                POST("user", handler::save)
            }
        }
        resources(FORWARD, ClassPathResource("static/"))
    }
}