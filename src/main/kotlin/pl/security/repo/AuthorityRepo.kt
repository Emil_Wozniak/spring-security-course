package pl.security.repo

import org.springframework.data.jpa.repository.JpaRepository
import pl.security.model.Authority

interface AuthorityRepo: JpaRepository<Authority, Long>