package pl.security.repo

import org.springframework.data.jpa.repository.JpaRepository
import pl.security.model.OneTimePassword
import java.util.*

interface OneTimePasswordRepo : JpaRepository<OneTimePassword, Long> {

    fun findByUsername(username: String): Optional<OneTimePassword>
}