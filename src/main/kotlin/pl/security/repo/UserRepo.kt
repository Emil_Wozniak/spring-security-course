package pl.security.repo

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import pl.security.model.User
import java.util.*

interface UserRepo: JpaRepository<User, Long> {

    @Query(value = "select user from User as user where user.username=:username")
    fun findByUsername(username: String): Optional<User>

    fun findUserByUsername(username: String): Optional<User>
}