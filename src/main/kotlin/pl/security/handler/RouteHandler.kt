package pl.security.handler

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.badRequest
import org.springframework.web.servlet.function.ServerResponse.ok
import pl.security.model.User
import pl.security.security.model.SecurityUser
import pl.security.service.JpaUserDetailsService

@Service
class RouteHandler(
    private val userService: JpaUserDetailsService,
    private val passwordEncoder: PasswordEncoder,
) {

    val log: Logger = LoggerFactory.getLogger(this::class.java.simpleName)

    fun save(request: ServerRequest): ServerResponse =
        request.body(User::class.java).let {
            it.password = passwordEncoder.encode(it.password)
            SecurityUser(it)
                .let { principals ->
                    userService.createUser(principals)
                    it
                }
                .let { entity -> ok().body(entity) }
        }

    fun hello(request: ServerRequest): ServerResponse = request.principal().run {
        if (isPresent) ok().body("Hello ${get().name}")
        else badRequest().body("Hello ")
    }

    fun test(request: ServerRequest): ServerResponse = request.let {
        if (it.principal().isEmpty) log.error("Received ${request.body(String::class.java)}")
        ok().body("You called Test endpoint successfully")
    }
}