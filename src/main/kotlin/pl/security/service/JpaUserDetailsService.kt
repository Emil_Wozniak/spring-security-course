package pl.security.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.provisioning.UserDetailsManager
import org.springframework.stereotype.Component
import pl.security.model.Authority
import pl.security.model.User
import pl.security.repo.UserRepo
import pl.security.security.model.SecurityUser
import javax.transaction.Transactional

@Component
class JpaUserDetailsService(private val repository: UserRepo) : UserDetailsManager {
    val log: Logger = LoggerFactory.getLogger(this::class.java.simpleName)

    override fun loadUserByUsername(username: String?): UserDetails? =
        try {
            val user = username?.let { findUser(it) }
            SecurityUser(user)
        } catch (error: Exception) {
            log.error(error.message)
            null
        }

    private fun findUser(username: String) = repository
        .findUserByUsername(username)
        .orElseThrow { UsernameNotFoundException("User not found \uD83D\uDE26") }

    override fun createUser(user: UserDetails): Unit = User(user).let {
        val authorities = getAuthorities(it)
        it.authorities = authorities
        repository.save(it)
    }

    @Transactional
    override fun updateUser(user: UserDetails) = repository.findUserByUsername(user.username)
        .ifPresent {
            it.password = user.password
            it.enabled = it.enabled
            repository.save(it)
        }

    override fun deleteUser(username: String) = repository.findUserByUsername(username)
        .ifPresent {
            repository.delete(it)
        }

    override fun changePassword(oldPassword: String?, newPassword: String?) {
    }

    override fun userExists(username: String): Boolean =
        repository.findUserByUsername(username).isPresent

    private fun getAuthorities(user: User) = user.authorities
        .map { Authority(null, user, it.authority) }
}
