package pl.security.security.manager

import org.springframework.stereotype.Component

@Component
class TokenManager(
    val tokens: MutableList<String> = ArrayList()
) {
    fun add(token: String) = tokens.add(token)
    operator fun TokenManager.plus(token: String) = this.tokens.add(token)
    operator fun TokenManager.contains(token: String) = this.tokens.contains(token)
}


