package pl.security.security.authentication

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class OTPAuthentication(
    principal: Any?,
    credentials: Any?,
    authorities: Collection<GrantedAuthority>?
) : UsernamePasswordAuthenticationToken(
    principal,
    credentials,
    authorities
)
