package pl.security.security.provider

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import pl.security.security.authentication.UsernamePasswordAuthentication
import pl.security.service.JpaUserDetailsService

@Component
class UsernamePasswordProvider(
    private val encoder: PasswordEncoder,
    private val details: JpaUserDetailsService
) : AuthenticationProvider {

    val log: Logger = LoggerFactory.getLogger(this::class.java.simpleName)

    /**
     * Authentication logic:
     *
     * 1. If request is authenticated should return fully Authentication instance.
     * 2. If request is authenticated should throw AuthenticationException.
     * 3. If Authentication is not supported return null.
     * @see AuthenticationException
     * @see Authentication
     */
    override fun authenticate(authentication: Authentication?): Authentication? =
        try {
            authentication?.name
                .let { details.loadUserByUsername(it) }
                .let {
                    it?.run {
                        val credentials = authentication?.credentials as String
                        if (encoder.matches(credentials, password)) createAuth()
                        else throw BadCredentialsException("Bad credentials \uD83D\uDE26")
                    }
                }
        } catch (error: Exception) {
            log.error(error.message)
            null
        }

    /**
     * @param authentication type of Authentication
     */
    override fun supports(authentication: Class<*>?): Boolean {
        return UsernamePasswordAuthentication::class.java == authentication
    }
}

fun UserDetails.createAuth() = UsernamePasswordAuthentication(
    username,
    password,
    authorities
)
