package pl.security.security.provider

import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.stereotype.Component
import pl.security.security.authentication.KeyAuthentication

@Component
class KeyProvider(@Value(value = "\${key}") private val key: String) : AuthenticationProvider {

    /**
     * Authentication logic:
     *
     * 1. If request is authenticated should return fully Authentication instance.
     * 2. If request is authenticated should throw AuthenticationException.
     * 3. If Authentication is not supported return null.
     * @see AuthenticationException
     * @see Authentication
     */
    override fun authenticate(authentication: Authentication?): Authentication = authentication?.name.let {
        return if (it.equals(key)) KeyAuthentication(null, null, null)
        else throw BadCredentialsException("Doesn't match the key")
    }

    /**
     * @param authentication type of Authentication
     */
    override fun supports(authentication: Class<*>?): Boolean =
        KeyAuthentication::class.java == authentication
}