package pl.security.security.provider

import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import pl.security.config.constant.EMOJI
import pl.security.repo.AuthorityRepo
import pl.security.repo.OneTimePasswordRepo
import pl.security.security.authentication.OTPAuthentication

@Component
class OTPAuthenticationProvider(
    private val repo: OneTimePasswordRepo,
    private val auths: AuthorityRepo
) : AuthenticationProvider {

    override fun authenticate(authentication: Authentication?): Authentication =
        authentication?.name
            .let { repo.findByUsername(it!!) }
            .run {
                val otp = authentication?.credentials as String
                val entity = get()
                if (isPresent && entity.oneTimePassword == otp) createAuthentication(authentication, otp)
                else throw BadCredentialsException("Bad credentials $EMOJI")
            }

    override fun supports(authentication: Class<*>?): Boolean =
        OTPAuthentication::class.java == authentication

    private fun createAuthentication(
        authentication: Authentication,
        otp: String
    ) = OTPAuthentication(authentication.name, otp, authorities())

    private fun authorities() = listOf(auths.getOne(1L))
}