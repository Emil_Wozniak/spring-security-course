package pl.security.security.provider

import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import pl.security.config.constant.EMOJI
import pl.security.security.authentication.TokenAuthentication
import pl.security.security.manager.TokenManager

@Component
class TokenAuthProvider(
    private val tokenManager: TokenManager
) : AuthenticationProvider {

    override fun authenticate(authentication: Authentication?): Authentication? =
        authentication?.run {
            val token = name as String
            val exists = token in tokenManager.tokens
            if (exists) TokenAuthentication(token, null, null)
            else throw BadCredentialsException("Unknown token $EMOJI")
        }

    override fun supports(authentication: Class<*>?): Boolean =
        TokenAuthentication::class.java == authentication
}