@file:Suppress("UNCHECKED_CAST")

package pl.security.security.model

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import pl.security.model.Authority
import pl.security.model.User

class SecurityUser(private val user: User?) : UserDetails {

    override fun getPassword(): String = user!!.password

    override fun getUsername(): String = user!!.username

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    override fun isCredentialsNonExpired(): Boolean = true

    override fun isEnabled(): Boolean = true

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> =
        listOf(Authority(1L, user, "read"))
                as MutableCollection<out GrantedAuthority>
}