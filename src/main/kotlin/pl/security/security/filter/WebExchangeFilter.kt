package pl.security.security.filter

import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

@Component
class WebExchangeFilter : WebFilter {
    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> =
        if (exchange.request.uri.path == "/form") chain
            .filter(
                exchange
                    .mutate()
                    .request { it.path("/styles/*.css").build() }
                    .request { it.path("/login.html").build() }
                    .build()
            )
        else chain.filter(exchange)

}