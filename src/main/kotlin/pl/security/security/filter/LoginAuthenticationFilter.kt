package pl.security.security.filter

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder.getContext
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import pl.security.config.constant.AUTHORIZATION
import pl.security.config.constant.ONE_TIME_PASSWORD
import pl.security.config.constant.PASSWORD
import pl.security.config.constant.USERNAME
import pl.security.model.OneTimePassword
import pl.security.repo.OneTimePasswordRepo
import pl.security.security.authentication.KeyAuthentication
import pl.security.security.authentication.OTPAuthentication
import pl.security.security.authentication.UsernamePasswordAuthentication
import pl.security.security.manager.TokenManager
import java.util.*
import java.util.UUID.randomUUID
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletResponse.SC_FORBIDDEN
import javax.servlet.http.HttpServletResponse.SC_OK

/**
 * First layer to access the endpoints, it will consume user login and password or
 *
 */
@Component
class LoginAuthenticationFilter constructor(
    private val manager: AuthenticationManager,
    private val tokenManager: TokenManager,
    private val oneTimePassRepo: OneTimePasswordRepo
) : OncePerRequestFilter() {

    val log: Logger = LoggerFactory.getLogger(this::class.java.simpleName)

    /**
     * return a path that is not filtered during http servlet request.
     */
    override fun shouldNotFilter(request: HttpServletRequest): Boolean =
        request.servletPath != "/login" || Resources.invoke(request)

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        resolveProviderStrategy(request, response).also {
            if (it != null && it.isAuthenticated) {
                getContext().authentication = it
                response.status = SC_OK
            } else response.status = SC_FORBIDDEN
        }
    }

    private fun resolveProviderStrategy(request: HttpServletRequest, response: HttpServletResponse): Authentication? =
        request.run {
            val key = getHeader(AUTHORIZATION)
            val username = getHeader(USERNAME)
            val pwd = getHeader(PASSWORD)
            val otp = getHeader(ONE_TIME_PASSWORD)
            try {
                when {
                    (otp == null) -> authorizeByPassword(username, pwd)
                    (pwd == null) -> authorizeByOneTimePassword(username, otp, response)
                    (key != null) -> authorizeByKey(key)
                    else -> throw BadCredentialsException("No credentials found")
                }
            } catch (error: Exception) {
                log.error(error.message)
                null
            }
        }

    /**
     * Find a proper authorization provider, authenticate it, create and store
     * OneTimePassword and then return the provider.
     */
    private fun authorizeByPassword(username: String, password: String) =
        UsernamePasswordAuthentication(username, password, null)
            .let {
                val authenticate = manager.authenticate(it)
                val code = (Random().nextInt(9999) + 1000).toString()
                val entity = OneTimePassword(null, username, code)
                log.info(code)
                oneTimePassRepo.save(entity)
                authenticate
            }

    /**
     * Find a proper authorization provider, authenticate it, create and set Authorization
     * Header and then return the provider.
     */
    private fun authorizeByOneTimePassword(
        username: String,
        otp: String,
        response: HttpServletResponse
    ): Authentication =
        OTPAuthentication(username, otp, null)
            .let {
                val authenticate = manager.authenticate(it)
                val token = randomUUID().toString()
                response.setHeader(AUTHORIZATION, token)
                tokenManager.tokens.add(token)
                authenticate
            }

    private fun authorizeByKey(authorization: String) =
        KeyAuthentication(authorization, null, null)
            .let { manager.authenticate(it) }
}