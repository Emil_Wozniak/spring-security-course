package pl.security.security.filter

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder.getContext
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import pl.security.config.constant.AUTHORIZATION
import pl.security.security.authentication.TokenAuthentication
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletResponse.SC_FORBIDDEN
import javax.servlet.http.HttpServletResponse.SC_OK

/**
 * Provides logic for second layer of authorization strategy. After satisfy login endpoint all endpoints
 * will be available by providing Header with correct token.
 */
@Component
class TokenAuthenticationFilter constructor(
    private val manager: AuthenticationManager
) : OncePerRequestFilter() {

    val log: Logger = LoggerFactory.getLogger(this::class.java.simpleName)

    /**
     * allows any servlet path other than /login
     */
    override fun shouldNotFilter(request: HttpServletRequest): Boolean =
        request.servletPath == "/login" || Resources.invoke(request)

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) =
        request.run {
            resolveToken().let {
                if (it != null) {
                    getContext().authentication = it
                    chain.doFilter(request, response)
                    response.status = SC_OK
                } else response.status = SC_FORBIDDEN
            }
        }

    private fun HttpServletRequest.resolveToken(): Authentication? = try {
        val token = getHeader(AUTHORIZATION)
        val authentication = TokenAuthentication(token, null, null)
        val authenticate = manager.authenticate(authentication)
        authenticate
    } catch (error: Exception) {
        log.error(error.message)
        null
    }
}