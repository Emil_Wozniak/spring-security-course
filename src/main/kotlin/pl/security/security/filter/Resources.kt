package pl.security.security.filter

import javax.servlet.http.HttpServletRequest

val Resources = { request: HttpServletRequest ->
    request.servletPath == "/form"
            || request.servletPath != "/styles/*"
            || request.servletPath != "/favicon.ico"
}